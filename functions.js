'use strict';


//*************************Iteración #1: Buscar el máximo********************************/
function sum(numberOne , numberTwo) {
    let mayor=0;
    if (numberOne>=numberTwo) {
        mayor = numberOne;
        console.log(numberOne,' es mayor o igual que ',numberTwo)
    } else {
        mayor = numberTwo;
        console.log(numberTwo,' es mayor o igual que ',numberOne);
    }
    return(mayor);
  }

console.log (sum(10,10));

//***************************Iteración #2: Buscar la palabra más larga*****************/
const avengers = ['Hulk', 'Thor', 'IronMan', 'Captain A.', 'Spiderman', 'Captain M.'];
function findLongestWord(param) {
    let person = avengers[0];
    let avengersLengt= person.length;
    for (let i = 1; i < (param.length)-1; i++) {
        if (param[i].length>avengersLengt) {
            person = param[i];
            avengersLengt = param[i].length;    
        }
    }
    return(person)    ;
 
}

console.log (findLongestWord(avengers));



//***************************Iteración #3: CALCULAR UNA SUMA*****************/

const numbers = [1, 2, 3, 5, 45, 37, 58];

function sumAll(param) {
  let sum=0;
  for (let i = 0; i < (param.length); i++) {
    sum=sum+param[i];   
  }
  return(sum);
}

console.log (sumAll(numbers));


//***************************Iteración #4: CALCULAR EL PROMEDIO*****************/

const numberss = [12, 21, 38, 5, 45, 37, 6];
function average(param) {
    let sum=0;
  for (let i = 0; i < (param.length); i++) {
    sum=sum+param[i];   
  }
  return(sum/(param.length));
}

console.log (average(numberss));
//***************************Iteración #5: VALORES UNICOS*****************/
const duplicates = [
    'sushi',
    'pizza',
    'burger',
    'potatoe',
    'pasta',
    'ice-cream',
    'pizza',
    'chicken',
    'onion rings',
    'pasta',
    'soda'
  ];
  function removeDuplicates(param) {
    for (let i = 0; i < param.length; i++) {
        for (let j = i+1; j < param.length; j++) {
            if (param[i]===param[j]) {
               param.splice(j, 1)
            }
        }
        
    }
    return(param);
  }

  console.log(removeDuplicates(duplicates));


//***************************Iteración #6: Calcular promedio de strings*****************/
const mixedElements = [6, 1, 'Rayo', 1, 'vallecano', '10', 'upgrade', 8, 'hub'];
function averageWord(param) {
    let sum=0;
    for (let i = 0; i < (param.length); i++) {
        if ( typeof param[i]==='number') {
            sum=sum+param[i];
        } else {
            sum=sum+param[i].length;
        }
    }
    return(sum);
}

console.log(averageWord(mixedElements));


//***************************Iteración #7: Buscador de nombres*****************/

const nameFinder = [
  'Peter',
  'Steve',
  'Tony',
  'Natasha',
  'Clint',
  'Logan',
  'Xabier',
  'Bruce',
  'Peggy',
  'Jessica',
  'Marc'
];
function finderName(param,name) {
  let index = param.indexOf(name)
  if (index==-1) {
    return console.log(false) 
  } else {
    return console.log(true, index);
  }
}
finderName(nameFinder, 'Clintttt');

//***************************Iteración #8: Contador de repeticiones*****************/
const counterWords = [
  'code',
  'repeat',
  'eat',
  'sleep',
  'code',
  'enjoy',
  'sleep',
  'code',
  'enjoy',
  'upgrade',
  'code'
];
debugger;

function repeatCounter(param) {
  let counter = {}; //se declara un objeto vacia
                      //Ejemplo de objeto se compone de elementos **clave:valor**
                      //var myAvenger = {
                        //name: 'Captain America',
                        //power: 80,
                        //creator: 'Stan Lee'
                      //};

  for (let i = 0; i < param.length; i++) {
    if (param[i] in counter) { //si la palabra con indice i del array recibido esta en el objeto counter
      counter[param[i]]++;    // sumamos uno al valor de la clave param[i]
    } else {                  // si la palabra con indice i del array recibido "NO ESTA" esta en el objeto counter
      counter[param[i]] = 1;  // añadimos el elemento al objeto clave==param[i] y valor sera 1 por ser la primera vez que aparce.
    }
  }
  return console.log(counter);
}

repeatCounter(counterWords);